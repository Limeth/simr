# SimR

This is the implementation part of my [master's thesis on "Simulating the phenomena of altered states of consciousness using virtual reality" available on GitHub](https://github.com/Limeth/master-thesis/).

## Running

### Download a precompiled binary

Download the precompiled binary from [GitHub](https://github.com/Limeth/master-thesis/releases/tag/printed). The executable is contained within the archive that is split into 3 separate files that need to be downloaded separately and then unzipped.

### Launching

Open the unzipped directory and launch `SimR.exe`.

### Controls

The VR application requires no controllers besides the VR headset itself. The virtual scene contains nothing to interact with, the user is simply supposed to observe the environment and how it affects them.

- `Shift + T`: Launches the active/test scenario
- `Shift + C`: Launches the control scenario

## Development

See the directory `Plugins` for the C++ (CPU) and HLSL (GPU) part of the implementation.
The rest is done in Unreal Engine blueprints and materials, accessible from the Unreal Engine editor.

### Prerequisites

- A SteamVR-compatible VR headset
- Unreal Engine 4.27.2 and the respective version of Visual Studio 2019

### Launching from source

1. Open the project in Unreal Engine 4.27.2
2. In the top toolbar, click on Play -> VR Preview
