#include "/Plugin/ShaderDirectory/Common.ush"

struct custom_function_t {
    // wobbly bijective function 
	float R(float x, float r, float A_min, float A_max)
	{
		float w = exp(-0.5 * Common.pow2(x / r));
		return lerp(A_max, A_min, w);
	}

	// This is the implementation of the first attempt of depth perception distortion.
	// It is not used in the final application.
	float wobble(float x, float B, float r, float A_min, float A_max)
	{
		float a = exp(B);
		float abs_x = abs(x);
		float log_a_abs_x = log(abs_x) / B;
		float modulated_term = (1.0 / PI) * sin(PI * log_a_abs_x);
		float unmodulated_term = log_a_abs_x;
		float exp = modulated_term * R(abs_x, r, A_min, A_max) + unmodulated_term;
		return sign(x) * pow(a, exp);
	}
} CustomFunction;

float3 rel_pos = absolute_world_position - camera_position;
float rel_pos_norm = length(rel_pos);
float wobbly_norm = CustomFunction.wobble(rel_pos_norm, B, r, A_min, A_max);
float wobbly_norm_diff = wobbly_norm - rel_pos_norm;
float3 wobbly_rel_pos = rel_pos * (wobbly_norm_diff / rel_pos_norm);

return wobbly_rel_pos;