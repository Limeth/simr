// Test project fo SceneViewExtension / RDG Shader Basic setup
// Copyright 2021 Ossi Luoto
// 
// SceneVEComponent - The SceneViewExtensionBase. Place this in the editor to an empty actor 

#include "SceneVEComponent.h"
#include "SceneVEProcess.h"

// Functions needed for SceneViewExtension
FTestSceneExtension::FTestSceneExtension(const FAutoRegister& AutoRegister) : FSceneViewExtensionBase(AutoRegister)
{
	UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Autoregister"));
}

// Defer the destruction of `IPooledRenderTarget`s to the render thread
FTestSceneExtension::~FTestSceneExtension() {
	AsyncTask(ENamedThreads::Type::ActualRenderingThread, [RTs = MoveTemp(this->PreviousFrameRTs)] {
		(void)RTs;
	});
}

void FTestSceneExtension::BeginRenderViewFamily(FSceneViewFamily& InViewFamily)
{
	//UE_LOG(LogTemp, Log, TEXT("TestSceneViewExtension: Begin Render View Family"));
}

// This is called every frame, use to subscribe where needed
void FTestSceneExtension::SubscribeToPostProcessingPass(EPostProcessingPass PassId, FAfterPassCallbackDelegateArray& InOutPassCallbacks, bool bIsPassEnabled)
{
	if (PassId == EPostProcessingPass::MotionBlur) // chosen pass to insert as
	{
		InOutPassCallbacks.Add(FAfterPassCallbackDelegate::CreateRaw(this, &FTestSceneExtension::TestPostProcessPass_RT));
	}
}

// Extension comes with the Graphbuilder and SceneView plus set of PostProcessMaterialInputs - more on these at SceneViewExtension.h
// This is just an empty helper function now if we need to validate data or make other alterations
// I think otherwise we could directly subscribe the Process Function to the PassCallBack

FScreenPassTexture FTestSceneExtension::TestPostProcessPass_RT(FRDGBuilder& GraphBuilder, const FSceneView& SceneView, const FPostProcessMaterialInputs& InOutInputs)
{
	FScreenPassTexture SceneTexture = FSceneVEProcess::AddSceneVETestPass(*this, GraphBuilder, SceneView, InOutInputs);
	return SceneTexture;
}

USceneVEComponent::USceneVEComponent(const FObjectInitializer& ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void USceneVEComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Component BeginPlay! %xll"), this);
	CreateSceneViewExtension();
}

void USceneVEComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Component EndPlay! %xll"), this);

	if (this->TestSceneExtension.IsValid()) {
		checkf(this->TestSceneExtension.IsUnique(), TEXT("TestSceneViewExtension: View extension not unique during EndPlay. %xll"), this);
		this->TestSceneExtension.Reset();
	}
}

// On a separate function to hook f.ex. for in editor creation etc.
void USceneVEComponent::CreateSceneViewExtension()
{
	if (!TestSceneExtension.IsValid()) {
		this->TestSceneExtension = FSceneViewExtensions::NewExtension<FTestSceneExtension>();
		UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Scene Extension Created!"));
	}
}


// Called every frame
void USceneVEComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	this->TestSceneExtension->Update(*this);
}

void FTestSceneExtension::Update(USceneVEComponent& component) {
	this->coefficient_accumulation = component.coefficient_accumulation;
	this->coefficient_opacity = component.coefficient_opacity;
	this->unsharp_mask_radius = component.unsharp_mask_radius;
	this->unsharp_mask_intensity = component.unsharp_mask_intensity;

	this->ComputeUnsharpMaskParams();
}

float SampleNormalDist(float stdev, float x) {
	float variance = stdev * stdev;
	return expf((x * x) / (-2.0f * variance)) / sqrtf(2.0f * PI * variance);
}

// Compensate for HMD resolution.
// Ignores FOV.
float GetResolutionCompensation() {
	if (GEngine->XRSystem == nullptr || GEngine->XRSystem->GetHMDDevice() == nullptr) {
		return 1.0;
	}

	IHeadMountedDisplay* HMD = GEngine->XRSystem->GetHMDDevice();
	FIntPoint RenderTargetResolution = HMD->GetIdealRenderTargetSize();
	//UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Res: %ld %ld"), RenderTargetResolution.X, RenderTargetResolution.Y);
	//UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Pixel density: %f"), GEngine->XRSystem->GetHMDDevice()->GetPixelDenity());
	return ((float) RenderTargetResolution.Y) / 2056.0; // calibrated to HTC Vive vertical resolution at 140% (warp compensation) * 150% (SteamVR auto supersampling setting) scale
}

void FTestSceneExtension::ComputeUnsharpMaskParams() {
	if (this->unsharp_mask_radius > 0.0) {
		float ResolutionCompensation = GetResolutionCompensation();
		//UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: ResolutionCompensation: %f"), ResolutionCompensation);
		float stdev = this->unsharp_mask_radius * ResolutionCompensation;
		this->unsharp_mask_matrix_len = (uint32) ceilf(stdev * 3.0f);

		// Warn when required radius exceeds the maximum, but do not terminate.
		ensureMsgf(this->unsharp_mask_matrix_len <= GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN, TEXT("Required gaussian blur convolution matrix length (%ld) exceeds the maximum (%ld)."), this->unsharp_mask_matrix_len, GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN);
		
		if (this->unsharp_mask_matrix_len > GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN) {
			this->unsharp_mask_matrix_len = GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN;
		}

		float sum = 0.0;

		for (uint32 x = 0; x < this->unsharp_mask_matrix_len; x++) {
			float sample = SampleNormalDist(stdev, (float)x);
			this->unsharp_mask_matrix[x] = sample;
			sum += sample * (x == 0 ? 1.0f : 2.0f); // count samples at x != 0 twice, because they are used twice in the convolution 
		}

		// Normalize so that the sum equals 1
		for (uint32 x = 0; x < this->unsharp_mask_matrix_len; x++) {
			this->unsharp_mask_matrix[x] /= sum;
		}
	}
	else {
		this->unsharp_mask_matrix_len = 1;
		this->unsharp_mask_matrix[0] = 1.0;
	}
}