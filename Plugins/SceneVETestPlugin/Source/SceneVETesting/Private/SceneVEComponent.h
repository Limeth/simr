// Test project fo SceneViewExtension / RDG Shader Basic setup
// Copyright 2021 Ossi Luoto
// 
// SceneVEComponent - The SceneViewExtensionBase. Place this in the editor to an empty actor 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "RenderGraphUtils.h"
#include "IHeadMountedDisplay.h"
#include "IXRTrackingSystem.h"

#include "SceneViewExtension.h"
#include "PostProcess/PostProcessing.h"
#include "PostProcess\PostProcessMaterial.h"

#include "SceneVEComponent.generated.h"

#define GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN 32

// Use FSceneViewExtensionBase to extend hook properly
class FTestSceneExtension : public FSceneViewExtensionBase
{
public:
	TArray<TRefCountPtr<IPooledRenderTarget>> PreviousFrameRTs;
	// All unique weights of the convolution matrix sampled at [0, 1, ..., len - 1]
	// So that the sum of all weights [-(len - 1), ..., len - 1] equals 1.
	TStaticArray<float, GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN> unsharp_mask_matrix;
	uint32 unsharp_mask_matrix_len = 0;

	float coefficient_accumulation = 0.0;
	float coefficient_opacity = 0.0;
	float unsharp_mask_radius = 0.0;
	float unsharp_mask_intensity = 0.0;

public:
	FTestSceneExtension(const FAutoRegister& AutoRegister);
	~FTestSceneExtension();
	void Update(USceneVEComponent& component);
	void ComputeUnsharpMaskParams();

	// These must all be set
public:
	virtual void SetupViewFamily(FSceneViewFamily& InViewFamily) override {};
	virtual void SetupView(FSceneViewFamily& InViewFamily, FSceneView& InView) override {};
	virtual void BeginRenderViewFamily(FSceneViewFamily& InViewFamily) override; // like component tick
	virtual void PreRenderViewFamily_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily) override {};
	virtual void PreRenderView_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneView& InView) override {};
	virtual void SubscribeToPostProcessingPass(EPostProcessingPass PassId, FAfterPassCallbackDelegateArray& InOutPassCallbacks, bool bIsPassEnabled);

	// This is our actual hook function
	FScreenPassTexture TestPostProcessPass_RT(FRDGBuilder& GraphBuilder, const FSceneView& View, const FPostProcessMaterialInputs& InOutInputs);

private:
};

// SceneVEComponent. Simple spawnable component to be place in the editor to an empty or other actor

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SCENEVETESTING_API USceneVEComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/// <summary>
	/// The blending coefficient of the color accumulation buffer.
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin="0.0", ClampMax="1.0"));
	float coefficient_accumulation = 0.5;

	/// <summary>
	/// The opacity of the accumulation buffer.
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = "0.0", ClampMax = "1.0"));
	float coefficient_opacity = 0.5;

	/// <summary>
	/// The radius of the sharpening filter.
	/// This is the standard deviation of the gaussian blur used in the unsharp mask.
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = "0.0"));
	float unsharp_mask_radius = 4.0;

	/// <summary>
	/// The radius of the sharpening filter.
	/// This is the standard deviation of the gaussian blur used in the unsharp mask.
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = "0.0"));
	float unsharp_mask_intensity = 1.0;

public:
	USceneVEComponent(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void CreateSceneViewExtension();

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	// this is the way to store the SceneExtensionView to keep it safe from not being destroyed - from: SceneViewExtension.h
	TSharedPtr<class FTestSceneExtension, ESPMode::ThreadSafe> TestSceneExtension;
};
