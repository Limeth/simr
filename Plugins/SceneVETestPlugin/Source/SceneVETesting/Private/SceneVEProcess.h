// Test project fo SceneExtensionView / RDG Shader Basic setup
// Copyright 2021 Ossi Luoto
// 
// SceneVEProcess.cpp - Actual RDG hook and Shader Loading

#pragma once
#include "SceneVEComponent.h"

#include "ScreenPass.h"
#include "Shader.h"
#include "GlobalShader.h"
#include "ShaderParameterUtils.h"
#include "ShaderParameterStruct.h"

#include "RenderGraphUtils.h"

// Parameter Declaration
BEGIN_SHADER_PARAMETER_STRUCT(FCustomFragmentShaderCommonParameters, )
	SHADER_PARAMETER(float, Time)
	SHADER_PARAMETER(float, DeltaTime)
	SHADER_PARAMETER(float, CoefficientAccumulation)
	SHADER_PARAMETER(float, CoefficientOpacity)
	SHADER_PARAMETER(uint32, UniqueConvolutionMatrixLen)
	SHADER_PARAMETER_ARRAY(float, UniqueConvolutionMatrix, [GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN])
	SHADER_PARAMETER_SAMPLER(SamplerState, InputSampler)
END_SHADER_PARAMETER_STRUCT()

BEGIN_SHADER_PARAMETER_STRUCT(FVerticalBlurParameters, )
	SHADER_PARAMETER_STRUCT(FCustomFragmentShaderCommonParameters, CustomCommon)
	SHADER_PARAMETER_RDG_TEXTURE(Texture2D, InputTexture)
	RENDER_TARGET_BINDING_SLOTS()
END_SHADER_PARAMETER_STRUCT()

BEGIN_SHADER_PARAMETER_STRUCT(FUnsharpMaskAndTemporalBlurParameters, )
	SHADER_PARAMETER_STRUCT(FCustomFragmentShaderCommonParameters, CustomCommon)
	SHADER_PARAMETER(float, UnsharpMaskIntensity)
	SHADER_PARAMETER_RDG_TEXTURE(Texture2D, InputTexture)
	SHADER_PARAMETER_RDG_TEXTURE(Texture2D, VerticalBlurTexture)
	SHADER_PARAMETER_RDG_TEXTURE(Texture2D, AccumulatorTexture)
	RENDER_TARGET_BINDING_SLOTS()
END_SHADER_PARAMETER_STRUCT()

// Declare Test Shader Class

class SCENEVETESTING_API FCustomPostProcessVS : public FGlobalShader
{
public:
	// Vertex Shader Declaration
	DECLARE_GLOBAL_SHADER(FCustomPostProcessVS)

	// Basic shader stuff
	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN"), GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN);
	}

	FCustomPostProcessVS() {}

	FCustomPostProcessVS(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
		: FGlobalShader(Initializer) {}
};

class SCENEVETESTING_API FUnsharpMaskAndTemporalBlurPS : public FGlobalShader
{
public:
	// RDG Pixel Shader Declaration
	DECLARE_GLOBAL_SHADER(FUnsharpMaskAndTemporalBlurPS)
	SHADER_USE_PARAMETER_STRUCT_WITH_LEGACY_BASE(FUnsharpMaskAndTemporalBlurPS, FGlobalShader)

	// Use the parameters from previously delcared struct
	using FParameters = FUnsharpMaskAndTemporalBlurParameters;

	// Basic shader stuff
	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN"), GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN);
	}
};

class SCENEVETESTING_API FVerticalBlurPS : public FGlobalShader
{
public:
	// RDG Pixel Shader Declaration
	DECLARE_GLOBAL_SHADER(FVerticalBlurPS)
	SHADER_USE_PARAMETER_STRUCT_WITH_LEGACY_BASE(FVerticalBlurPS, FGlobalShader)

	// Use the parameters from previously delcared struct
	using FParameters = FVerticalBlurParameters;

	// Basic shader stuff
	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN"), GAUSSIAN_BLUR_UNIQUE_CONVOLUTION_MATRIX_MAX_LEN);
	}
};

class FSceneVEProcess
{
public:
	// Hook to the SceneViewExtension Base
	static FScreenPassTexture AddSceneVETestPass(FTestSceneExtension& extension, FRDGBuilder& GraphBuilder, const FSceneView& View, const FPostProcessMaterialInputs& Inputs);

private:
};