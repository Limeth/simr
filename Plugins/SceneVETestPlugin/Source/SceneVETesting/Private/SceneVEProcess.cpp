// Test project fo SceneViewExtension / RDG Shader Basic setup
// Copyright 2021 Ossi Luoto
// 
// SceneVEProcess.cpp - Actual RDG hook and Shader Loading

#include "SceneVEProcess.h"

// Shader implementation Macro doesn't work on .h file so load them here
IMPLEMENT_GLOBAL_SHADER(FCustomPostProcessVS, "/Plugins/SceneVETestPlugin/CustomPostProcessVS.usf", "MainVS", SF_Vertex);
IMPLEMENT_GLOBAL_SHADER(FUnsharpMaskAndTemporalBlurPS, "/Plugins/SceneVETestPlugin/UnsharpMaskAndTemporalBlurPS.usf", "MainPS", SF_Pixel);
IMPLEMENT_GLOBAL_SHADER(FVerticalBlurPS, "/Plugins/SceneVETestPlugin/VerticalBlurPS.usf", "MainPS", SF_Pixel);

// Draw Screen Pass function copied directly from the engine OpenColorIODisplayExtension.cpp
// copy start
namespace {
	template<typename TSetupFunction>
	void DrawScreenPass(
		FRHICommandListImmediate& RHICmdList,
		const FSceneView& View,
		const FScreenPassTextureViewport& OutputViewport,
		const FScreenPassTextureViewport& InputViewport,
		const FScreenPassPipelineState& PipelineState,
		TSetupFunction SetupFunction)
	{
		PipelineState.Validate();

		const FIntRect InputRect = InputViewport.Rect;
		const FIntPoint InputSize = InputViewport.Extent;
		const FIntRect OutputRect = OutputViewport.Rect;
		const FIntPoint OutputSize = OutputRect.Size();

		RHICmdList.SetViewport(OutputRect.Min.X, OutputRect.Min.Y, 0.0f, OutputRect.Max.X, OutputRect.Max.Y, 1.0f);

		SetScreenPassPipelineState(RHICmdList, PipelineState);

		// Setting up buffers.
		SetupFunction(RHICmdList);

		FIntPoint LocalOutputPos(FIntPoint::ZeroValue);
		FIntPoint LocalOutputSize(OutputSize);
		EDrawRectangleFlags DrawRectangleFlags = EDRF_UseTriangleOptimization;

		DrawPostProcessPass(
			RHICmdList,
			LocalOutputPos.X, LocalOutputPos.Y, LocalOutputSize.X, LocalOutputSize.Y,
			InputRect.Min.X, InputRect.Min.Y, InputRect.Width(), InputRect.Height(),
			OutputSize,
			InputSize,
			PipelineState.VertexShader,
			View.StereoPass,
			false,
			DrawRectangleFlags);
	}

}
// copy end

FScreenPassTexture FSceneVEProcess::AddSceneVETestPass(FTestSceneExtension& extension, FRDGBuilder& GraphBuilder, const FSceneView& SceneView, const FPostProcessMaterialInputs& Inputs)
{
	// SceneViewExtension gives SceneView, not ViewInfo so we need to setup some basics ourself
	const FSceneViewFamily& ViewFamily = *SceneView.Family;
	const ERHIFeatureLevel::Type FeatureLevel = SceneView.GetFeatureLevel();

	uint32 ViewIndex = GEngine->StereoRenderingDevice.IsValid() ? GEngine->StereoRenderingDevice->GetViewIndexForPass(SceneView.StereoPass) : 0;

	//UE_LOG(LogTemp, Warning, TEXT("TestSceneViewExtension: VIEW INDEX: %d"), ViewIndex);

	const FScreenPassTexture& SceneColor = Inputs.Textures[(uint32)EPostProcessMaterialInput::SceneColor];

	if (!SceneColor.IsValid())
	{
		return SceneColor;
	}

	// Here starts the RDG stuff
	RDG_EVENT_SCOPE(GraphBuilder, "SceneVETestPass");
	{
		// Accesspoint to our Shaders
		FGlobalShaderMap* GlobalShaderMap = GetGlobalShaderMap(ViewFamily.GetFeatureLevel());

		// Template Render Target to use as main output
		FRDGTextureDesc OutputDesc = SceneColor.Texture->Desc;
		OutputDesc.Flags |= TexCreate_RenderTargetable;
		OutputDesc.Flags &= ~TexCreate_ResolveTargetable;
		OutputDesc.ClearValue = FClearValueBinding(FLinearColor(0.0, 0.0, 0.0, 0.0));
		FScreenPassRenderTarget TemplateRenderTarget;

		// Check if this destination is the last one on the post process pipeline
		if (Inputs.OverrideOutput.IsValid())
		{
			TemplateRenderTarget = Inputs.OverrideOutput;
		}
		else
		// Otherwise make a template RenderTarget
		{
			FRDGTexture* TemplateRenderTargetTexture = GraphBuilder.CreateTexture(OutputDesc, TEXT("templateRenderTargetTexture"));
			TemplateRenderTarget = FScreenPassRenderTarget(TemplateRenderTargetTexture, SceneColor.ViewRect, ERenderTargetLoadAction::EClear);
		}

		/*
		uint32 ConvolutionBufferLen = 10; // TODO
		FRDGBufferDesc ConvolutionBufferDesc = FRDGBufferDesc::CreateBufferDesc(sizeof(float), ConvolutionBufferLen);
		FRDGBufferRef ConvolutionBuffer = GraphBuilder.CreateBuffer(ConvolutionBufferDesc, TEXT("ConvolutionBuffer"));

		GraphBuilder.RegisterExternalBuffer();
		FRDGExternalBuffer
		*/

		FRDGTexture* VerticalBlurRenderTargetTexture = GraphBuilder.CreateTexture(OutputDesc, TEXT("VerticalBlurRenderTargetTexture"));
		FScreenPassRenderTarget VerticalBlurRenderTarget = FScreenPassRenderTarget(VerticalBlurRenderTargetTexture, SceneColor.ViewRect, ERenderTargetLoadAction::EClear);
		
		FRDGTexture* AccumulationRenderTargetTexture = GraphBuilder.CreateTexture(OutputDesc, TEXT("AccumulationRenderTargetTexture"));
		FScreenPassRenderTarget AccumulationRenderTarget = FScreenPassRenderTarget(AccumulationRenderTargetTexture, SceneColor.ViewRect, ERenderTargetLoadAction::EClear);

		// Create textures that will store the previous frame
		auto& PreviousFrameRTs = extension.PreviousFrameRTs;

		bool FirstFrame = ViewIndex >= (uint32) PreviousFrameRTs.Num();

		while (ViewIndex >= (uint32) PreviousFrameRTs.Num()) {
			PreviousFrameRTs.AddDefaulted();
			UE_LOG(LogTemp, Display, TEXT("TestSceneViewExtension: Added previous frame RT #%d"), ViewIndex);
		}

		FRDGTexture* PreviousFrameTexture = FirstFrame ? SceneColor.Texture : GraphBuilder.RegisterExternalTexture(PreviousFrameRTs[ViewIndex]);
		auto& PreviousFrameRT = PreviousFrameRTs[ViewIndex];
		
		// The Viewport in source and destination might be different
		const FScreenPassTextureViewport SceneColorViewport(SceneColor);
		const FScreenPassTextureViewport TemplateRenderTargetViewport(TemplateRenderTarget);

		FScreenPassRenderTarget SceneColorRenderTarget(SceneColor, ERenderTargetLoadAction::ELoad);

		// We need these for the GraphBuilder AddPass
		FRHIBlendState* DefaultBlendState = FScreenPassPipelineState::FDefaultBlendState::GetRHI();
		FRHIDepthStencilState* DepthStencilState = FScreenPassPipelineState::FDefaultDepthStencilState::GetRHI();

		// Common shader parameters
		FCustomFragmentShaderCommonParameters PassParametersCommon{};

		PassParametersCommon.Time = GWorld->GetTimeSeconds();
		PassParametersCommon.DeltaTime = FApp::GetDeltaTime();
		PassParametersCommon.CoefficientAccumulation = extension.coefficient_accumulation;
		PassParametersCommon.CoefficientOpacity = extension.coefficient_opacity;
		PassParametersCommon.UniqueConvolutionMatrixLen = extension.unsharp_mask_matrix_len;

		for (uint32 i = 0; i < extension.unsharp_mask_matrix_len; i++) {
			PassParametersCommon.UniqueConvolutionMatrix[i] = extension.unsharp_mask_matrix[i];
		}

		PassParametersCommon.InputSampler = TStaticSamplerState<>::GetRHI();

		// First pass: Vertical blur
		{
			// Get the assigned shaders
			TShaderMapRef<FCustomPostProcessVS> VertexShader(GlobalShaderMap);
			TShaderMapRef<FVerticalBlurPS> PixelShader(GlobalShaderMap);

			// Pass parameters
			FVerticalBlurParameters* PassParameters = GraphBuilder.AllocParameters<FVerticalBlurParameters>();
			PassParameters->CustomCommon = PassParametersCommon;

			PassParameters->InputTexture = SceneColorRenderTarget.Texture;
			PassParameters->RenderTargets[0] = VerticalBlurRenderTarget.GetRenderTargetBinding();

			// Schedule pass
			GraphBuilder.AddPass(
				RDG_EVENT_NAME("VerticalBlurPass"),
				PassParameters,
				ERDGPassFlags::Raster,
				[
					&SceneView, VertexShader, PixelShader, DefaultBlendState, DepthStencilState,
					SceneColorViewport, TemplateRenderTargetViewport, PassParameters
				](FRHICommandListImmediate& RHICmdList)
				{
					DrawScreenPass(
						RHICmdList,
						SceneView,
						TemplateRenderTargetViewport,
						SceneColorViewport,
						FScreenPassPipelineState(VertexShader, PixelShader, DefaultBlendState, DepthStencilState),
						[&](FRHICommandListImmediate& RHICmdList)
						{
							SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);
						});
				}
			);
		}

		// Second pass: Horizontal blur, unsharp mask, temporal blur
		{
			// Get the assigned shaders
			TShaderMapRef<FCustomPostProcessVS> VertexShader(GlobalShaderMap);
			TShaderMapRef<FUnsharpMaskAndTemporalBlurPS> PixelShader(GlobalShaderMap);

			// Pass parameters
			FUnsharpMaskAndTemporalBlurParameters* PassParameters = GraphBuilder.AllocParameters<FUnsharpMaskAndTemporalBlurParameters>();
			PassParameters->CustomCommon = PassParametersCommon;

			PassParameters->UnsharpMaskIntensity = extension.unsharp_mask_intensity;
			PassParameters->InputTexture = SceneColorRenderTarget.Texture;
			PassParameters->VerticalBlurTexture = VerticalBlurRenderTarget.Texture;
			PassParameters->AccumulatorTexture = PreviousFrameTexture;
			PassParameters->RenderTargets[0] = TemplateRenderTarget.GetRenderTargetBinding();
			PassParameters->RenderTargets[1] = AccumulationRenderTarget.GetRenderTargetBinding();

			// Schedule pass
			GraphBuilder.AddPass(
				RDG_EVENT_NAME("UnsharpMaskAndTemporalBlurPass"),
				PassParameters,
				ERDGPassFlags::Raster,
				[
					&SceneView, VertexShader, PixelShader, DefaultBlendState, DepthStencilState,
					SceneColorViewport, TemplateRenderTargetViewport, PassParameters
				](FRHICommandListImmediate& RHICmdList)
				{
					DrawScreenPass(
						RHICmdList,
						SceneView,
						TemplateRenderTargetViewport,
						SceneColorViewport,
						FScreenPassPipelineState(VertexShader, PixelShader, DefaultBlendState, DepthStencilState),
						[&](FRHICommandListImmediate& RHICmdList)
						{
							SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);
						});
				}
			);

			GraphBuilder.QueueTextureExtraction(AccumulationRenderTarget.Texture, &PreviousFrameRT);
		}

		// Return the result
		return MoveTemp(TemplateRenderTarget);
	}
}

